# How to: Teensy 4.1 Development With Arduino IDE

Learn how to program Teensy 4.1 using Arduino IDE.

Holger Zahnleiter, 2021-11-15

## Prerequisites

For this guide you need:

- Teensyduino IDE 1.8.16 (https://www.pjrc.com/teensy/td_download.html)
- Teensy 4.1 board
- Micro-USB cable

## Download Teensyduino

Downloads for various platforms are provided by the Teensy makers here: https://www.pjrc.com/teensy/td_download.html

By the time of this writing the following versions are the most current ones:

- Arduino 1.8.16
- Teensyduino 1.55

## Install Teensyduino

### macOS

For macOS, a complete, pre-configured Arduino app is provided.
So, no need to have a current version of the Arduino IDE installed.
Download from here: https://www.pjrc.com/teensy/td_155/Teensyduino_MacOS_Catalina.zip.
Unpack and copy to the `Programs` folder.
The app is called `Teensyduino.app`.
Double click to start (as usual).

Teensyduino is a replacement for the original `Arduino.app`.
Boards and libraries you may have installed are taken over and are available in Teensyduino.
Therefore, remove `Arduino.app` from the programs folder should it be installed.

### Linux (Jetson Nano)

Basically, follow the guide given here: https://www.pjrc.com/teensy/td_download.html.

You need to have a current version of the Arduino IDE installed.
In my case this is 1.8.16.
I have installed it under `~/opt/arduino-1.8.16`.

Download Linux udev rules from here: https://www.pjrc.com/teensy/00-teensy.rules.
I was using a web browser for this.
It automatically downloads files to `~/Downloads/`.
When using `wget` or `curl` your files may end up in other folders.
In a console type
```shell
cd ~/Downloads/
sudo cp 00-teensy.rules /etc/udev/rules.d/
```

Now download Teensyduino from here: https://www.pjrc.com/teensy/td_155/TeensyduinoInstall.linuxaarch64.
Which Linux package to download depends on your hardware architecture, e.g. ARM, x86 etc.
In my case it is aarch64 for my Jetson Nano.
On the console, still under `~/Downloads/` type
```shell
chmod 755 TeensyduinoInstall.linuxaarch64
./TeensyduinoInstall.linuxaarch64
```

Now follow the steps in the Teensyduino installer.
Most of the time the only thing you need to do is to press "Next".

<img src="teensiduino_install_1.png" width="60%"/>

That is good.
Seems like the udev rules were installed successfully before.

<img src="teensiduino_install_2.png" width="60%"/>

This requires some input from you.
Select the folder where your Arduino IDE resides.
In my case `~/opt/arduino-1.8.16/`.

<img src="teensiduino_install_3.png" width="60%"/>

<img src="teensiduino_install_4.png" width="60%"/>

<img src="teensiduino_install_5.png" width="60%"/>

<img src="teensiduino_install_6.png" width="60%"/>

Now you can plug in your Teensy and run the Arduino IDE.

## A First Program

1. From the "File/Examples" menu pick a simple program named `Blink`.
2. From the "Tools/Board" menu pick "Teensy 4.1" board.
3. Also from the "Tools/Port" menu pick "/dev/cu.usbmodem105635801 Serial (Teensy 4.1)"
   - The actual port name depends on your operating system and may vary.
      For example, on my Linux machine this is "/dev/ttyACM0 Serial (Teensy 4.1)".
4. Klick on the "Compile and Upload" button.
   - A window, that is specific to Teensy development, pops up.
     This can be ignored.
     The sketch will be compiled, uploaded and started as usual.
     Just like with your "ordinary" Arduino.
5. You should now see Teensys built-in LED blink.

<img src="./upload.png"/>

Ignore this window.
Upload will succeed and program will be execute without pressing any buttons.

<img src="./teensy_popup.png"/>

With its LED lit, Teensy almost looks like HAL 9000 or a Cylon Centurion - scary.

<img src="./scary_teensy.jpeg"/>
